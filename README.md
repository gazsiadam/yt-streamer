# YtStremer

A project intended to create easily new live stream events on Youtube. 
Since Youtube implemented the feature to reuse the settings from a completed live stream, this project makes no sense anymore.

To make it work, you need to create two things in the Google API Console (https://console.cloud.google.com)
1. Client ID for Web application
2. API key for Youtube
After creating these API keys, these should be updated in the App.ts file!

What works and can be reused:
1. Oauth2 login with Google.
2. Request Channel data from Youtube of the signed-in user.
3. Get the list of videos of the signed-in user.  

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
