import { HttpClient } from '@angular/common/http';
import { AppComponent } from '../app/app.component';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { App } from '../app/config/App';

import { YtCredentialService } from '../youtube/yt-login.service'; // should be without '../youtube/'

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';

import { Video } from '../app/entities/video';

@Injectable()
export class YtService {
    // channel related data
    private channelData: Object;
    private videoLists: Object;

    constructor(private http: HttpClient, private ytCS: YtCredentialService) { }

    getChannelDataUrl(): string {
        let url = App.YT_BASE_URL + 'channels';
        url += '?part=contentDetails';
        url += '&mine=true';
        url += '&access_token=' + this.ytCS.getAccessToken();
        url += '&key= ' + App.YT_API_KEY;

        return url;
    }

    getVideos(): Observable<any> {
        let url = App.YT_BASE_URL + 'playlistItems';
        url += '?part=snippet';
        url += '&access_token=' + this.ytCS.getAccessToken();
        url += '&key=' + App.YT_API_KEY;
        url += '&maxResults=50';
        url += '&resultsPerPage=50';
        url += '&playlistId=';

        let result = this.http.get(this.getChannelDataUrl()).map((response: Response) => {
            console.log(response);
            this.channelData = response;

            return this.channelData;
        }).flatMap((chanelData) => this.http.get(url + chanelData['items'][0].contentDetails.relatedPlaylists.uploads));
        result.subscribe(res => this.videoLists = res);

        return result;
    }

    getPlaylists(): Observable<any[]> {
        return null; // TODO: not implemented
    }

    createPlaylist() {
        // TODO: not implemented
    }

}
