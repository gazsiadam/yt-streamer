import { HttpClient } from '@angular/common/http';
import { AppComponent } from '../app/app.component';
import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { App } from '../app/config/App';

import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/map';

@Injectable()
export class YtCredentialService {

    constructor(private http: HttpClient, private router: Router) { }

    public getAccessToken(): string {
        if (this.isAccessTokenExpired()) {
            this.refreshTokens();
        }

        let tokens = JSON.parse(localStorage.getItem('currentUser'));
        return tokens['accessToken'];
    }

    public login() {
        let url = 'https://accounts.google.com/o/oauth2/v2/auth?redirect_uri=' + App.REDIRECT_URL;
        url += '&prompt=consent&response_type=code&client_id=' + App.YT_CLIENT_ID;
        url += '&scope=https://www.googleapis.com/auth/youtube+https://www.googleapis.com/auth/youtube.upload+https://www.googleapis.com/auth/youtubepartner&access_type=offline';

        // redirect the user to the google's oauth2 page
        window.location.href = url;
    }

    public exchangeCode(code: String): any {
        let url = 'https://www.googleapis.com/oauth2/v4/token?code=' + code;
        url += '&redirect_uri=' + App.REDIRECT_URL;
        url += '&client_id=' + App.YT_CLIENT_ID;
        url += '&client_secret=' + App.YT_CLIENT_SECRET + '&scope=&grant_type=authorization_code';

        return this.http.post(url, {}).subscribe(res => {
            this.updateCredentials(res);

            // redirect to home
            this.router.navigate(['/home']);
        });
    }

    public refreshTokens() {
        let url = 'https://accounts.google.com/o/oauth2/v4/token?';
        url += 'client_id=' + App.YT_CLIENT_ID;
        url += '&client_secret=' + App.YT_CLIENT_SECRET + '&scope=&grant_type=authorization_code';
        url += '&grant_type=refresh_token&refresh_token=' + this.getRefreshToken();

        this.http.post(url, {}).subscribe(res => {
            this.updateCredentials(res);
        });
    }

    public isLoggedIn(): boolean {
        let currentUser = localStorage.getItem('currentUser');
        let accessToken = currentUser && JSON.parse(currentUser)['accessToken'];

        if (accessToken) {
            return true;
        }

        return false;
    }

    public logout() {
        localStorage.removeItem('currentUser');
    }

    private isAccessTokenExpired(): boolean {
        let tokens = JSON.parse(localStorage.getItem('currentUser'));
        let lastUpdate = new Date(tokens['lastUpdate']);
        let expiry = tokens['expiresIn'];

        let d = lastUpdate.setMinutes(lastUpdate.getMinutes() + expiry);
        let now = new Date().getTime();

        if (d < now) {
            return true;
        }

        return false;
    }

    private updateCredentials(credentials) {
        let accessToken = credentials['access_token'];
        let refreshToken = credentials['refresh_token'];
        let expiresIn = credentials['expires_in'];

        localStorage.removeItem('currentUser'); // remove previous tokens
        localStorage.setItem('currentUser', JSON.stringify({
            accessToken: accessToken,
            refreshToken: refreshToken,
            expiresIn: expiresIn,
            lastUpdate: new Date()
        }));
    }

    private getRefreshToken(): string {
        let tokens = JSON.parse(localStorage.getItem('currentUser'));

        if (tokens) {
            return tokens['refreshToken'];
        }

        return null;
    }

}
