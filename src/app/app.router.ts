import {Routes} from '@angular/router';

import {AppComponent} from './app.component';
import {YtHomeComponent} from './components/yt-home/yt-home.component';
import {YtLoginComponent} from './components/yt-login/yt-login.component';
import {YtVideoDetailComponent} from './components/yt-video-detail/yt-video-detail.component';

export const router: Routes = [
    {path: '', redirectTo: '/login', pathMatch: 'full'},
    {path: 'home', component: YtHomeComponent},
    {path: 'login', component: YtLoginComponent},
    {path: 'video', component: YtVideoDetailComponent}
];
