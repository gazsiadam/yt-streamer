import 'rxjs/add/operator/map';
import {Http} from '@angular/http';
import {YtService} from '../youtube/yt.service';
import {YtCredentialService} from '../youtube/yt-login.service';
import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private ytCS: YtCredentialService) {}

    ngOnInit() {}

}
