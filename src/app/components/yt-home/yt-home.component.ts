import {YtCredentialService} from '../../../youtube/yt-login.service';
import {YtService} from '../../../youtube/yt.service';
import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';

@Component({
    selector: 'app-yt-home',
    templateUrl: './yt-home.component.html',
    styleUrls: ['./yt-home.component.css']
})
export class YtHomeComponent implements OnInit {

    videos: any[];

    constructor(private yt: YtService, private ytCS: YtCredentialService, private router: Router) {}

    ngOnInit() {
        if (!this.ytCS.isLoggedIn()) {
            this.router.navigate(['/login']);
        } else {
            this.yt.getVideos().subscribe(videos => {
                this.videos = videos.items;
            });
        }
    }

    public openVideo(video) {
        console.log('Open video: ' + video);
        this.router.navigate(['/video', video]);
    }

    public logout() {
        this.ytCS.logout();
        // redirect to login
        this.router.navigate(['/login']);
    }

}
