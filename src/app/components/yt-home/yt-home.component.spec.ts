import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YtHomeComponent } from './yt-home.component';

describe('YtHomeComponent', () => {
  let component: YtHomeComponent;
  let fixture: ComponentFixture<YtHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
