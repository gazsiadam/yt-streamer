import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YtVideoDetailComponent } from './yt-video-detail.component';

describe('YtVideoDetailComponent', () => {
  let component: YtVideoDetailComponent;
  let fixture: ComponentFixture<YtVideoDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtVideoDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtVideoDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
