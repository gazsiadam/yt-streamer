import 'rxjs/add/operator/map';
import {Http} from '@angular/http';
import {YtService} from '../../../youtube/yt.service';
import {YtCredentialService} from '../../../youtube/yt-login.service';
import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
    selector: 'app-yt-login',
    templateUrl: './yt-login.component.html',
    styleUrls: ['./yt-login.component.css']
})
export class YtLoginComponent implements OnInit {

    constructor(private router: Router, private activatedRoute: ActivatedRoute, private ytCS: YtCredentialService) {}

    ngOnInit() {
        if (!this.ytCS.isLoggedIn()) {
            this.router.routerState.root.queryParams.subscribe((param: Params) => {
                let code = param['code'];

                if (code != null) {
                    this.ytCS.exchangeCode(code);
                }
            });
        } else {
            this.router.navigate(['/home']);
        }
    }


    public login() {
        this.ytCS.logout();
        this.ytCS.login();
    }

}
