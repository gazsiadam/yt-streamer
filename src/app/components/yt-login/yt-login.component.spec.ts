import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YtLoginComponent } from './yt-login.component';

describe('YtLoginComponent', () => {
  let component: YtLoginComponent;
  let fixture: ComponentFixture<YtLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YtLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YtLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
